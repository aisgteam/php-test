<?php

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/lib/Commands.php';
use Workerman\Worker;
use Workerman\Connection\AsyncTcpConnection;

$ws_worker = new Worker("websocket://0.0.0.0:3000");
$ws_worker->count = 4;

$users = [];

$command = new Commands();

$ws_worker->onConnect = function($connection) use (&$users, $ws_worker) {
    $connection->onWebSocketConnect = function($wsconnection) use ($ws_worker, &$users) {
        if(isset($_GET['client_id']) && isset($_GET['task_id'])) {

            $client_id = $_GET['client_id'];
            $task_id = $_GET['task_id'];

            $wsconnection->uid = $task_id;
            $wsconnection->user_id = $client_id;

            if(isset($users[$client_id])) {
                $user = $users[$client_id];
            } else {
                $user = [];
            }

            $user[$task_id] = $wsconnection;
            $users[$client_id] = $user;
            $ws_worker->adminServer->send(json_encode([
                'worker' => 'WebWorker',
                'action' => 'connect',
                'user_id' => $client_id,
                'task_id' => $task_id,
            ]));
        }
    };

};
$ws_worker->onClose = function($connection) use(&$users, $ws_worker) {
    if(isset($users[$connection->user_id])) {
        $user = $users[$connection->user_id];
        if(isset($user[$connection->uid])) {
            unset($user[$connection->uid]);
            if(empty($user)) {
                unset($users[$connection->user_id]);
            }
        }
    }
    $ws_worker->adminServer->send(json_encode([
        'worker' => 'WebWorker',
        'action' => 'disconnect',
        'user_id' => $connection->user_id,
        'task_id' => $connection->uid
    ]));
};

$ws_worker->onWorkerStart = function() use (&$users, $ws_worker) {

    $connection = new AsyncTcpConnection("tcp://127.0.0.1:1234");
    $connection->onMessage = function($connection, $data) use (&$users) {

        $data = json_decode($data);
        switch ($data->user) {
            case 'all':
                foreach($users as $tasks) {
                    foreach($tasks as $webconnection) {
                        $webconnection->send($data->message);
                    }
                }
                break;
            default:
                if (isset($users[$data->user])) {
                    if(isset($data->task)) {
                        if(isset($users[$data->user][$data->task])) {
                            $webconnection = $users[$data->user][$data->task];
                            $webconnection->send($data->message);
                        }
                    } else {
                        foreach($users[$data->user] as $webconnection) {
                            $webconnection->send($data->message);
                        }
                    }
                }
        }
    };
    $connection->connect();
    $ws_worker->adminServer = $connection;
};

$tcp_worker = new Worker("tcp://127.0.0.1:1234");

$tcp_worker->onMessage = function($connection, $data) use ($tcp_worker, &$command, &$users)
{
    $json = json_decode($data, true);
    if(av('worker', $json) == 'WebWorker' && av('action', $json) == 'connect') {
        $user_id = av('user_id', $json);
        $task_id = av('task_id', $json);
        if(isset($users[$user_id])) {
            $user = $users[$user_id];
        } else {
            $user = [];
        }
        $user[] = $task_id;
        $users[$user_id] = $user;
    } else if(av('worker', $json) == 'WebWorker' && av('action', $json) == 'disconnect') {
        $user_id = av('user_id', $json);
        $task_id = av('task_id', $json);
        if(isset($users[$user_id])) {
            $key = array_search($task_id, $users[$user_id]);
            if($key !== false) {
                unset($users[$user_id][$key]);
            }
            if(empty($users[$user_id])) {
                unset($users[$user_id]);
            }
        }
    } else {
        $res = $command->isSupportCommands($json);
        if($res) {
            if($command->isCommands($command->supportCommands['get users'], $json)) {
                $connection->send(json_encode([
                    'users' => array_keys($users)
                ]));
            }
            if($command->isCommands($command->supportCommands['get user tasks'], $json)) {
                $user = $json['get-all-user-task'];
                $connection->send(json_encode([
                    'tasks' => (isset($users[$user]) ? $users[$user] : [])
                ]));
            }
            if($command->isCommands($command->supportCommands['send message'], $json) || $command->isCommands($command->supportCommands['send message to task'], $json)) {
                $data = $json;
                $data['user'] = $json['send-message'];
                unset($data['send-message']);
                $data = json_encode($data);
                foreach($tcp_worker->connections as $id => $webconnection) {
                    if($connection->id != $id) {
                        $webconnection->send($data);
                    }
                }
            }
        }
    }
};

Worker::runAll();

function av($key, $array, $default = null)
{
    if(isset($array[$key])) {
        return $array[$key];
    }
    return $default;
}