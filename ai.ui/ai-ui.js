/**
 * (c) 2017 Dmitriy Dergachev
 * License: MIT
 */
(function() {
    'use strict';
    angular.module('ai.tpls.form', [
        "ai/template/form/text.html",
        "ai/template/form/password.html",
        "ai/template/form/file.html",
        "ai/template/form/dir.html",
        "ai/template/form/select.html",
        "ai/template/form/textarea.html",
        "ai/template/form/checkbox.html",
        "ai/template/form/radio.html"
    ]);
    angular.module('ai.tpls.grid', ["ai/template/grid/row.html"]);
    const aiui = angular.module('ai.ui', [
        'pascalprecht.translate',
        'ai.tpls.form',
        'ai.tpls.grid'
    ]);
    aiui.directive('aiRow', function() {
        return {
            templateUrl: 'ai/template/grid/row.html',
            replace: true,
            restrict: 'E',
            transclude: true,
            link: function(scope, elem, attrs) {}
        };
    });
    aiui.directive('aiForm', function() {
        return {
            template: '<form class="ai-form" ng-transclude></form>',
            replace: true,
            restrict: 'E',
            transclude: true,
            scope: {},
            link: function(scope, element, attrs) {}
        };
    });
    aiui.directive('aiFieldset', function() {
        return {
            template: '<div class="ai-fieldset" ng-transclude></div>',
            replace: true,
            restrict: 'E',
            transclude: true,
            scope: {},
            link: function(scope, element, attrs) {}
        }
    });
    aiui.directive('aiGroupField', function() {
        return {
            template: '<div class="ai-group-field" ng-transclude></div>',
            replace: true,
            restrict: 'E',
            transclude: true,
            scope: {},
            link: function(scope, element, attrs) {}
        };
    });
    aiui.directive('aiText', function($aiUid) {
        return {
            templateUrl: 'ai/template/form/text.html',
            replace: true,
            restrict: 'E',
            scope: {
                ngModel: '=?',
                id: '@',
                name: '@',
                label: '@',
                required: '@',
            },
            link: function(scope, element, attrs) {
                let input = element.find('input');
                if(scope.id === undefined) scope.id = $aiUid.get();
                if(scope.required !== undefined) input.attr('required', 'required');
                element.removeAttr('id')
                    .removeAttr('name')
                    .removeAttr('label')
                    .removeAttr('required');
            }
        };
    });
    aiui.directive('aiPassword', function($aiUid) {
        return {
            templateUrl: 'ai/template/form/password.html',
            replace: true,
            restrict: 'E',
            scope: {
                ngModel: '=?',
                id: '@',
                name: '@',
                label: '@',
                required: '@',
            },
            link: function(scope, element, attrs) {
                let input = element.find('input');
                if(scope.id === undefined) scope.id = $aiUid.get();
                if(scope.required !== undefined) input.attr('required', 'required');
                element.removeAttr('id')
                    .removeAttr('name')
                    .removeAttr('label')
                    .removeAttr('required');
            }
        };
    });
    aiui.directive('aiFile', function($aiUid, $translate) {
        return {
            templateUrl: 'ai/template/form/file.html',
            replace: true,
            restrict: 'E',
            scope: {
                ngModel: '=?',
                id: '@',
                name: '@',
                label: '@',
                required: '@',
                accept: '@',
                multiple: '@',
            },
            link: function(scope, element, attrs) {
                let input = element.find('input');
                if(scope.id === undefined) scope.id = $aiUid.get();
                if(scope.required !== undefined) input.attr('required', 'required');
                if(scope.accept !== undefined) input.attr('accept', scope.accept);
                if(scope.multiple !== undefined) input.attr('multiple', true);
                element.removeAttr('id')
                    .removeAttr('label')
                    .removeAttr('name')
                    .removeAttr('accept')
                    .removeAttr('required')
                    .removeAttr('multiple');
            },
            controller: function($scope, $element, $attrs) {
                let input = angular.element($element[0].querySelector('input.ai-file'));
                let label = angular.element($element[0].querySelector('label'));
                let files = [];
                input.bind('change', function(changeEvent) {
                    $scope.$apply(function() {
                        $scope.ngModel = [];
                    });
                    $scope.read = function(number) {
                        if(files[number] !== undefined) {
                            reader.readAsDataURL(files[number]);
                        }
                    };
                    let reader = new FileReader();
                    let i = 0;
                    reader.onload = function(loadEvent) {
                        $scope.$apply(function() {
                            $scope.ngModel[i]['data'] = loadEvent.target.result;
                            i++;
                            $scope.read(i);
                        });
                    };
                    files = changeEvent.target.files;
                    angular.forEach(files, function(file) {
                        $scope.$apply(function() {
                            $scope.ngModel.push({
                                name: file.name,
                                data: ''
                            });
                        });
                    });
                    $scope.read(i);
                    $scope.$watch('ngModel', function(newValue) {
                        let n = newValue.length;
                        if(n > 0) {
                            if(n > 1) {
                                $translate('Selected files:').then(function(data) {
                                    label.text(data + ' ' + n);
                                });
                            } else {
                                label.text(newValue[0].name);
                            }
                        } else {
                            $translate('Select file').then(function(data) {
                                label.text(data);
                            });
                            input.value = null;
                        }
                    });
                });
            }
        }
    });
    aiui.directive('aiDir', function($aiUid, $translate) {
        return {
            templateUrl: 'ai/template/form/dir.html',
            replace: true,
            restrict: 'E',
            scope: {
                ngModel: '=?',
                id: '@',
                name: '@',
                label: '@',
                required: '@',
            },
            link: function(scope, element, attrs) {
                let input = element.find('input');
                if(scope.id === undefined) scope.id = $aiUid.get();
                if(scope.required !== undefined) input.attr('required', 'required');
                element.removeAttr('id')
                    .removeAttr('name')
                    .removeAttr('label')
                    .removeAttr('required')
                    .removeAttr('multiple');
            },
            controller: function($scope, $element, $attrs) {
                let input = angular.element($element[0].querySelector('input.ai-dir'));
                let label = angular.element($element[0].querySelector('label'));
                let dirs = [];
                input.bind('change', function(changeEvent) {
                    $scope.$apply(function() {
                        $scope.ngModel = {
                            name: '',
                            path: ''
                        };
                    });
                    dirs = changeEvent.target.files;
                    angular.forEach(dirs, function(dir) {
                        $scope.$apply(function() {
                            $scope.ngModel.name = dir.name;
                            $scope.ngModel.path = dir.path;
                        });
                    });
                });
                $scope.$watchCollection('ngModel', function(newValue) {
                    if(newValue.name !== '' && newValue.path !== '') {
                        label.text(newValue.name);
                    } else {
                        $translate('Select folder').then(function (data) {
                            label.text(data);
                        });
                        input.value = null;
                    }
                });
            }
        }
    });
    aiui.directive('aiSelect', function($aiUid) {
        return {
            templateUrl: 'ai/template/form/select.html',
            replace: true,
            restrict: 'E',
            transclude: true,
            scope: {
                ngModel: '=?',
                id: '@',
                name: '@',
                label: '@',
                required: '@',
                multiple: '@',
            },
            link: function(scope, element, attrs) {
                let select = element.find('select');
                if(scope.id === undefined) scope.id = $aiUid.get();
                if(scope.required !== undefined) select.attr('required', 'required');
                if(scope.multiple !== undefined) input.attr('multiple', true);
                element.removeAttr('id')
                    .removeAttr('name')
                    .removeAttr('label')
                    .removeAttr('required')
                    .removeAttr('multiple');
            }
        }
    });
    aiui.directive('aiTextarea', function($aiUid) {
        return {
            templateUrl: 'ai/template/form/textarea.html',
            replace: true,
            restrict: 'E',
            transclude: true,
            scope: {
                ngModel: '=?',
                id: '@',
                name: '@',
                label: '@',
                required: '@',
            },
            link: function(scope, element, attrs) {
                let textarea = element.find('textarea');
                if(scope.id === undefined) scope.id = $aiUid.get();
                if(scope.required !== undefined) textarea.attr('required', 'required');
                element.removeAttr('id')
                    .removeAttr('name')
                    .removeAttr('label')
                    .removeAttr('required');
            }
        }
    });
    aiui.directive('aiCheckbox', function($aiUid) {
        return {
            templateUrl: 'ai/template/form/checkbox.html',
            replace: true,
            restrict: 'E',
            scope: {
                ngModel: '=?',
                id: '@',
                name: '@',
                label: '@',
                required: '@',
            },
            link: function(scope, element, attrs) {
                let input = element.find('input');
                if(scope.id === undefined) scope.id = $aiUid.get();
                if(scope.required !== undefined) input.attr('required', 'required');
                element.removeAttr('id')
                    .removeAttr('name')
                    .removeAttr('label')
                    .removeAttr('required');
            }
        }
    });
    aiui.directive('aiRadio', function($aiUid) {
        return {
            templateUrl: 'ai/template/form/radio.html',
            replace: true,
            restrict: 'E',
            scope: {
                ngModel: '=?',
                id: '@',
                name: '@',
                label: '@',
                value: '@',
                required: '@',
            },
            link: function(scope, element, attrs) {
                let input = element.find('input');
                if(scope.id === undefined) scope.id = $aiUid.get();
                if(scope.value !== undefined) input.attr('value', attrs.value);
                if(scope.required !== undefined) input.attr('required', 'required');
                element.removeAttr('id')
                    .removeAttr('name')
                    .removeAttr('label')
                    .removeAttr('required')
                    .removeAttr('value');
            }
        }
    });
    aiui.directive('aiSubmit', function() {
        return {
            template: '<input class="ai-submit" type="submit" />',
            replace: true,
            restrict: 'E',
            link: function(scope, element, attrs) {}
        };
    });
    aiui.directive('aiReset', function() {
        return {
            template: '<input class="ai-reset" type="reset" />',
            replace: true,
            restrict: 'E',
            link: function(scope, element, attrs) {}
        };
    });
    aiui.directive('aiButton', function() {
        return {
            template: '<button class="ai-button" ng-transclude></button>',
            replace: true,
            restrict: 'E',
            transclude: true,
            link: function(scope, element, attrs) {}
        };
    });
    aiui.directive('aiModals', function($rootScope, $aiModals) {
        return {
            restrict: 'E',
            link: function(scope, element, attrs) {
                scope.subview = null;
                element.on('click', function(event) {
                    if(element[0] !== event.target) {
                        return;
                    }
                    scope.$apply($aiModals.reject);
                });
                console.log('test modal');
                $rootScope.$on('$aiModals:open', function(event, modalType) {
                    scope.subview = modalType;
                });
                $rootScope.$on('$aiModals:close', function(event) {
                    scope.subview = null;
                })
            }
        }
    });
    // aiui.directive('aiModal', function($aiUid, $aiModal) {
    //     return {
    //         template: '<div class="ai-modal" ng-show="show" ng-transclude></div>',
    //         restrict: 'E',
    //         replace: false,
    //         transclude: true,
    //         link: function(scope, element, attrs) {
    //             scope.show = false;
    //             if(!attrs.id) attrs.id = $aiUid.get();
    //             angular.element(document.body).append(element);
    //             element.on('click', function($e) {
    //                 let target = angular.element($e.target);
    //                 if(target.hasClass('ai-modal')) {
    //                     scope.$evalAsync(Close);
    //                 }
    //             });
    //
    //             let modal = {
    //                 id: attrs.id,
    //                 open: Open,
    //                 close: Close
    //             };
    //             $aiModal.Add(modal);
    //
    //             scope.$on('$destroy', function() {
    //                 $aiModal.Remove(attrs.id);
    //                 element.remove();
    //             });
    //             function Open() {
    //                 scope.show = true;
    //                 angular.element(document.body).addClass('modal-open');
    //             }
    //             function Close() {
    //                 scope.show = false;
    //                 angular.element(document.body).removeClass('modal-open');
    //             }
    //         }
    //     }
    // });
    // aiui.directive('aiBtnCheckbox', function() {
    //     return {
    //         template: 'ai/template/form/btn-checkbox.html',
    //         replace: true,
    //         restrict: 'E',
    //         scope: {
    //             ngModel: '=?'
    //         },
    //         link: function(scope, element, attrs) {
    //             let input = element.find('input');
    //             let label = element.find('label');
    //         }
    //     }
    // });
    aiui.factory('$aiUid', function() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return {
            get: function() {
                return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
                    s4() + '-' + s4() + s4() + s4();
            }
        };
    });
    aiui.service('$aiModals', function($rootScope, $q) {
        let modal = {
            deferred: null,
            params: null
        };
        return {
            open: open,
            params: params,
            proceedTo: proceedTo,
            reject: reject,
            resolve: resolve
        };

        function open(type, params, pipeResponse) {
            let previousDeferred = modal.deferred;
            modal.deferred = $q.defer();
            modal.params = params;
            if(previousDeferred && pipeResponse) {
                modal.deferred.promise
                    .then(previousDeferred.resolve, previousDeferred.reject);
            } else if(previousDeferred) {
                previousDeferred.reject();
            }
            $rootScope.$emit('$aiModals:open', type);
            return modal.deferred.promise;
        }

        function params() {
            return modal.params || {};
        }

        function proceedTo(type, params) {
            return open(type, params, true);
        }

        function reject(reason) {
            if(!modal.deferred) {
                return;
            }
            modal.deferred.reject(reason);
            modal.deferred = modal.params = null;
            $rootScope.$emit('$aiModals:close');
        }

        function resolve(response) {
            if(!modal.deferred) {
                return;
            }
            modal.deferred.resolve(response);
            modal.deferred = modal.params = null;
            $rootScope.$emit('$aiModals:close');
        }
    });
    // aiui.factory('$aiModal', function() {
    //     let modals = [];
    //     let service = {};
    //
    //     service.Add = function(modal) {
    //         modals.push(modal);
    //     };
    //     service.Remove = function(id) {
    //         let modalToRemove = findWhere(id);
    //         modals.slice(modalToRemove, 1);
    //     };
    //     service.Open = function(id) {
    //         let modal = findWhere(id);
    //         modals[modal].open();
    //     };
    //     service.Close = function(id) {
    //         let modal = findWhere(id);
    //         modals[modal].close();
    //     };
    //     function findWhere(id) {
    //         let key = false;
    //         modals.forEach(function(modal, index) {
    //             if(modal.id === id) {
    //                 key = index;
    //             }
    //         });
    //         return key;
    //     }
    //     return service;
    // });
    aiui.config(['$translateProvider', function($translateProvider) {
        $translateProvider.translations('en', {
            'Select file': 'Select file',
            'Selected files:': 'Selected files:',
            'Select folder': 'Select folder',
            'Selected folders:': 'Selected folders:',
        });
        $translateProvider.translations('ru', {
            'Select file': 'Выберите файл',
            'Selected files:': 'Выбрано файлов:',
            'Select folder': 'Выберите папку',
            'Selected folders:': 'Выбраны папки:',
        });
        $translateProvider.registerAvailableLanguageKeys(['en', 'ru'], {
            'en-*': 'en',
            'ru-*': 'ru',
            '*': 'en'
        });
        $translateProvider.preferredLanguage(navigator.language || navigator.browserLanguage);
    }]);
    aiui.run(function() {
        !angular.$$csp().noInlineStyle && angular.element(document).find('head').prepend(
            `<style type="text/css">
            </style>`
        );
    });
    angular.module('ai/template/grid/row.html', []).run(['$templateCache', function($templateCache) {
        $templateCache.put('ai/template/grid/row.html',
            `<div class="ai-row" ng-transclude></div>`
        )
    }]);
    angular.module('ai/template/form/text.html', []).run(["$templateCache", function($templateCache) {
        $templateCache.put('ai/template/form/text.html',
            `<div class="ai-field">
                <input type="text" class="ai-text" name="{{name}}" id="{{id}}" ng-model="ngModel" />
                <label class="ai-label-text" for="{{id}}">{{label}}</label>
            </div>`
        )
    }]);
    angular.module('ai/template/form/password.html', []).run(["$templateCache", function($templateCache) {
        $templateCache.put('ai/template/form/password.html',
            `<div class="ai-field">
                <input type="password" class="ai-password" name="{{name}}" id="{{id}}" ng-model="ngModel" />
                <label class="ai-label-password" for="{{id}}">{{label}}</label>
            </div>`
        )
    }]);
    angular.module('ai/template/form/select.html', []).run(["$templateCache", function($templateCache) {
        $templateCache.put('ai/template/form/select.html',
            `<div class="ai-field">
                <select class="ai-select" name="{{name}}" id="{{id}}" ng-model="ngModel" ng-transclude></select>
                <label class="ai-label-select" for="{{id}}">{{label}}</label>
                <i></i>
            </div>`
        )
    }]);
    angular.module('ai/template/form/textarea.html', []).run(['$templateCache', function($templateCache) {
        $templateCache.put('ai/template/form/textarea.html',
            `<div class="ai-field">
                <textarea class="ai-textarea" name="{{name}}" id="{{id}}" ng-model="ngModel"></textarea>
                <label class="ai-label-textarea" for="{{id}}">{{label}}</label>
            </div>`
        )
    }]);
    angular.module('ai/template/form/file.html', []).run(["$templateCache", function($templateCache) {
        $templateCache.put('ai/template/form/file.html',
            `<div class="ai-field">
                <input type="file" class="ai-file" name="{{name}}" id="{{id}}" ng-model="ngModel" />
                <label class="ai-label-file" for="{{id}}">{{ 'Select file' | translate }}</label>
                <label class="ai-label-file" for="{{id}}">{{label}}</label>
            </div>`
        )
    }]);
    angular.module('ai/template/form/dir.html', []).run(["$templateCache", function($templateCache) {
        $templateCache.put('ai/template/form/dir.html',
            `<div class="ai-field">
                <input type="file" class="ai-dir" name="{{name}}" id="{{id}}" ng-model="ngModel" directory webkitdirectory/>
                <label class="ai-label-file" for="{{id}}">{{ 'Select folder' | translate }}</label>
                <label class="ai-label-file" for="{{id}}">{{label}}</label>
            </div>`
        )
    }]);
    angular.module('ai/template/form/checkbox.html', []).run(["$templateCache", function($templateCache) {
        $templateCache.put('ai/template/form/checkbox.html',
            `<div class="ai-checkbox">
                <input type="checkbox" class="ai-checkbox" name="{{name}}" id="{{id}}" ng-model="ngModel" />
                <label class="ai-label-checkbox" for="{{id}}">{{label}}</label>
            </div>`
        )
    }]);
    angular.module('ai/template/form/radio.html', []).run(["$templateCache", function($templateCache) {
        $templateCache.put('ai/template/form/radio.html',
            `<div class="ai-radio">
                <input type="radio" class="ai-radio" name="{{name}}" id="{{id}}" ng-model="ngModel"/>
                <label class="ai-label-radio" for="{{id}}">{{label}}</label>
            </div>`
        )
    }]);
})();