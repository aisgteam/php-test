# Angular Ai Ui #

Add UI component for you application

`npm install angular-ai-ui`

or

`yarn install angular-ai-ui`

## How to use ##

Need require file **ai-ui.js**

```js
var myApp = angular.module('myApp', ['ai.ui']);
```

### Use Form ###

```html
<ai-form id="login" method="post" action="you/url" enctype="multipart/form-data">
    <!-- html tags -->
</ai-form>
```

Will be replaced by

```html
<form class="ai-from" id="login" method="post" action="you/url" enctype="multipart/form-data">
    <!-- html tags -->
</form>
```

### Use Field ###

#### Use text field ####
```html
<ai-text id="id_field" label="Text to label" name="input_name" required ng-model="scope.name"></ai-text> 
```

Will be replaced by

```html
<div class="ai-form-group">
    <input type="text" id="id_field" class="ai-text" ng-model="ngModel" name="input_name" required="required"/>
    <label class="ai-label-text" for="id_field">Text to label</label>
</div>
```
#### Use password field ####
```html
<ai-password id="id_field" label="Text to label" name="input_name" required ng-model="scope.pass"></ai-password> 
```

Will be replaced by

```html
<div class="ai-form-group">
    <input type="password" id="id_field" class="ai-password" ng-model="ngModel" name="input_name" required="required"/>
    <label class="ai-label-password">Text to label</label>
</div>
```

#### Use file field ####
```html
<ai-file id="id_field" label="Text to label" name="input_name" accept="jpg" multiple required ng-model="scope.files"></ai-file>
```

Will be replaced by

```html
<div class="ai-form-group">
    <input type="file" id="id_field" class="ai-file" ng-model="ngModel" name="input_name" accept="jpg" multiple required="required" />
    <label class="ai-label-file">Text to label</label>
    <div class="ai-file-upload">{{ 'No file selected' | translate }}</div>
    <button class="btn btn-danger">{{ 'Clear' | translate }}</button>
    <button class="btn btn-primary">{{ 'View' | translate }}</button>
</div>
```

#### Use checkbox field ####
```html
<ai-checkbox id="id_field" label="Text to label" name="input_name"></ai-checkbox>
```