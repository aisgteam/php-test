<?php

require_once __DIR__ . '/lib/Commands.php';
require_once __DIR__ . '/lib/SocketConsoleClient.php';

$console = new SocketConsoleClient();
$console->ip('127.0.0.1')
    ->port(1234)
    ->connect();

if(count($argv) > 1) {
    $commands = array_slice($argv, 1);
    $msg = $console->parseCommand($commands);
} else {
    $console->showHelp();
    exit(1);
}

$msg = $console->parseCommand($commands);

$res = $console->isSupportCommands($msg);
$start = microtime(true);
if($res) {
//    while(microtime(true) - $start < 60) {
        $console->send($msg);
//        sleep(1);
//    }
} else {
    $console->showHelp();
    exit(1);
}