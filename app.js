var app = angular.module('myApp', [
    'ai.ui'
]);
app.controller('userChat', function($scope, $aiModals) {
    $scope.chat = false;
    $scope.user = {
        client_id: 15,
        task_id: 17
    };
    $scope.connected = false;
    $scope.ws = {
        url: "ws://sandbox.influ.su:3000/",
        messages: ''
    };
    $scope.connect = function() {
        var ws = new WebSocket(buildUrl($scope.ws.url, $scope.user));
        ws.onopen = function() {
            $scope.connected = true;
        };
        ws.onmessage = $scope.onmessage;
    };
    $scope.onmessage = function(event)
    {
        $scope.ws.messages += event.data + "\n";
        $scope.openChat();
    };
    $scope.openChat = function() {
        var promise = $aiModals.open('OpenChat', $scope.ws);
        promise.then(
            function handleResolve(response) {},
            function handleReject(error) {}
        );
    };
    function buildUrl(url, parameters) {
        var qs = "";
        for(var key in parameters) {
            var value = parameters[key];
            qs += encodeURIComponent(key) + "=" + encodeURIComponent(value) + "&";
        }
        if (qs.length > 0){
            qs = qs.substring(0, qs.length-1);
            url = url + "?" + qs;
        }
        return url;
    }
});
app.controller('Chat', function($scope, $aiModals) {
    $scope.data = $aiModals.params();
    $scope.add = function() {
        $aiModals.resolve($scope.data);
    };
    $scope.close = $aiModals.reject;
});