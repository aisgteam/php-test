<?php
/**
 * @author:             Dmitriy Dergachev (ArtMares)
 * @date:               06.04.2018
 */

class Commands
{
    public $supportCommands = [
        'get users' => ['get-all-users'],
        'get user tasks' => ['get-all-user-task'],
        'send message' => ['send-message', 'message'],
        'send message to task' => ['send-message', 'task', 'message']
    ];

    public function parseCommand($commands)
    {
        $msg = [];
        foreach($commands as $command) {
            $tmp = explode("=", $command);
            if(count($tmp) > 1) {
                $msg[$tmp[0]] = $tmp[1];
            } else {
                $msg[$tmp[0]] = true;
            }
        }
        return $msg;
    }

    public function isSupportCommands($data)
    {
        foreach($this->supportCommands as $commands) {
            $res = $this->isCommands($commands, $data);
            if($res == true) {
                return true;
            }
        }
        return false;
    }

    public function isCommand($command, $data)
    {
        return isset($data[$command]);
    }

    public function isCommands($commands, $data)
    {
        $result = 0;
        $n = count($commands);
        if($n > 0) {
            foreach($commands as $command) {
                if($this->isCommand($command, $data)) {
                    $result++;
                }
            }
            if($result == $n) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    public function showHelp()
    {
        echo PHP_EOL;
        echo 'Use commands:' . PHP_EOL;
        echo '    get-all-users' . PHP_EOL;
        echo '        Display the ID of all registered users' . PHP_EOL . PHP_EOL;
        echo '    get-all-user-task={task_id}' . PHP_EOL;
        echo '        Display the ID of all registered tasks of one user' . PHP_EOL . PHP_EOL;
        echo '    send-message=all message="Text message"' . PHP_EOL;
        echo '        Send a message all users' . PHP_EOL . PHP_EOL;
        echo '    send-message={user_id} message="Text message"' . PHP_EOL;
        echo '        Send a message to one user' . PHP_EOL . PHP_EOL;
        echo '    send-message={user_id} task={task_id} message="Text message"' . PHP_EOL;
        echo '        Send a message to one user in one task' . PHP_EOL . PHP_EOL;
    }
}