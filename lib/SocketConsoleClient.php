<?php
/**
 * @author:             Dmitriy Dergachev (ArtMares)
 * @date:               05.04.2018
 */

class SocketConsoleClient
{
    public $ip = '127.0.0.1';

    public $port = 3000;
    /** @var resource */
    protected $connection = false;

    protected $commands;

    protected $start_time;

    public function __construct()
    {
        $this->commands = new Commands();
    }

    public function ip($ip)
    {
        $this->ip = $ip;
        return $this;
    }

    public function port($port)
    {
        $this->port = (int)$port;
        return $this;
    }

    public function connect()
    {
        if(inet_pton($this->ip) !== false) {
            $localsocket = 'tcp://' . $this->ip . ($this->port > 0 ? ':' . $this->port : '');
            $this->connection = @stream_socket_client($localsocket, $errno, $errstr);
            if(!$this->connection) {
                echo 'No connection on ' . $localsocket . ' : ( ' . $errno . ' ) ' . $errstr . PHP_EOL;
                exit(250);
            }
            stream_set_blocking($this->connection, 0);
        } else {
            echo 'Not correct IP address: '. $this->ip . PHP_EOL;
            exit(250);
        }
    }

    public function send($data)
    {
        if($this->connection !== false) {
            echo PHP_EOL;
            $msg = $this->encode($data);
            $this->start_time = microtime(true);
            fwrite($this->connection, $msg . "\n");
            $response = $this->read();
            if($this->isGetAllUsers($data)) {
                $table = [];
                foreach($response->users as $user_id) {
                    $table[] = ['user_id' => $user_id];
                }
                echo 'Authorized Users:' . PHP_EOL;
                $this->printTable($table, ['user_id' => 'User ID']);
                $this->printRequestTime();
            }
            if($this->isGetUserTasks($data)) {
                $table = [];
                foreach($response->tasks as $task) {
                    $table[] = ['task_id' => $task];
                }
                echo 'User Tasks:' . PHP_EOL;
                $this->printTable($table, ['task_id' => 'Task ID']);
                $this->printRequestTime();
            }
        } else {
            echo 'No connection';
            exit(250);
        }
    }

    public function read()
    {
        $data = false;
        if($this->connection !== false) {
            while(!feof($this->connection)) {
                usleep(100);
                $data = fgets($this->connection);
                $data = $this->decode($data);
                return $data;
            }
        }
        return $data;
    }

    protected function encode($data)
    {
        return json_encode($data);
    }

    protected function decode($data)
    {
        return json_decode($data);
    }

    public function parseCommand($commands)
    {
        return $this->commands->parseCommand($commands);
    }

    public function isSupportCommands($data)
    {
        return $this->commands->isSupportCommands($data);
    }

    public function showHelp()
    {
        $this->commands->showHelp();
    }

    protected function isGetAllUsers($data)
    {
        return $this->commands->isCommands($this->commands->supportCommands['get users'], $data);
    }

    protected function isGetUserTasks($data)
    {
        return $this->commands->isCommands($this->commands->supportCommands['get user tasks'], $data);
    }

    protected function printTable($data, $headers = [])
    {
        $tl = "\u{250c}";
        $tc = "\u{252c}";
        $tr = "\u{2510}";
        $ml = "\u{251c}";
        $mc = "\u{253c}";
        $mr = "\u{2524}";
        $bl = "\u{2514}";
        $bc = "\u{2534}";
        $br = "\u{2518}";

        $column_width = 30;

        $columns_w = [];

        $columns = 0;

        $row_length = 0;

        $rows = count($data);

        if(!empty($headers)) {
            foreach($headers as $h_key => $h_val) {
                $len = strlen($h_val);
                if($len >= $column_width) {
                    $columns_w[$h_key] = $len + 1;
                } else {
                    $columns_w[$h_key] = $column_width;
                }
            }
            $columns = count($headers);
        }
        foreach($data as $row) {
            foreach($row as $key => $value) {
                $len = strlen($value);
                if ($len >= $column_width) {
                    $columns_w[$key] = $len + 1;
                } else {
                    $columns_w[$key] = $column_width;
                }
                $row_length += $columns_w[$key];
            }
        }

        echo $tl;
        $i = 0;
        foreach ($columns_w as $column) {
            $i++;
            echo str_pad('', $column, '-') . ($i < $columns ? $tc : '');
        }
        echo $tr . PHP_EOL;

        if(!empty($headers)) {
            echo '|';
            $i = 0;
            foreach ($headers as $h_key => $h_val) {
                $i++;
                echo str_pad(' ' . $h_val, $columns_w[$h_key], ' ', STR_PAD_RIGHT) . '|';
            }
            echo PHP_EOL;

            echo $ml;
            $i = 0;
            foreach ($columns_w as $column) {
                $i++;
                echo str_pad('', $column, '-') . ($i < $columns ? $mc : '');
            }
            echo $mr . PHP_EOL;
        }
        foreach($data as $row) {
            echo '|';
            foreach($row as $key => $value) {
                echo str_pad(' ' . $value, $columns_w[$key], ' ', STR_PAD_RIGHT) . '|';
            }
            echo PHP_EOL;

            echo $ml;
            $i = 0;
            foreach ($columns_w as $column) {
                $i++;
                echo str_pad('', $column, '-') . ($i < $columns ? $mc : '');
            }
            echo $mr . PHP_EOL;
        }

        echo '|' . str_pad(' Total: ' . $rows, ($row_length > 0 ? $row_length : $column_width), ' ') . '|' . PHP_EOL;

        echo $bl;
        $i = 0;
        foreach ($columns_w as $column) {
            $i++;
            echo str_pad('', $column, '-') . ($i < $columns ? $bc : '');
        }
        echo $br . PHP_EOL;
    }

    protected function printRequestTime()
    {
        echo 'Request time: ' . round(microtime(true) - $this->start_time, 4) . ' second' . PHP_EOL . PHP_EOL;
    }
}