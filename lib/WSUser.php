<?php
/**
 * @author:             Dmitriy Dergachev (ArtMares)
 * @date:               05.04.2018
 */

use Workerman\Connection\TcpConnection;

class WSUser
{
    public $client_id;

    protected $tasks = [];

    protected $connections = [];

    public function addTask(TcpConnection &$connection, $taskId)
    {
        $this->tasks[$connection->id] = $taskId;
        $this->connections[$connection->id] = $connection;
    }

    public function getTasks()
    {
        return array_values($this->tasks);
    }

    public function getConnections()
    {
        return $this->connections;
    }

    public function deleteTask($taskId)
    {
        $key = array_search($this->tasks, $taskId);
        if($key !== false) {
            $this->deleteTaskUseConnId($key);
        }
    }

    public function deleteTaskUseConnId($connId)
    {
        if(isset($this->tasks[$connId])) {
            unset($this->tasks[$connId], $this->connections[$connId]);
        }
    }

    public function isEmptyTasks()
    {
        return empty($this->tasks);
    }
}