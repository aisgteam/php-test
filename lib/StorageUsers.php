<?php
/**
 * @author:             Dmitriy Dergachev (ArtMares)
 * @date:               05.04.2018
 */

use Workerman\Connection\TcpConnection;

class StorageUsers
{
    /** @var User[] */
    protected $users = [];

    protected $links = [];

    public function add($user_id, $task_id, TcpConnection &$connection)
    {

    }

    public function get($id)
    {
        if(isset($this->users[$id])) {
            return $this->users[$id];
        }
        return false;
    }

    public function getThroughConnectionId($id)
    {
        if(isset($this->links[$id])) {
            $user_id = $this->links[$id];
            return $this->get($user_id);
        }
        return false;
    }
}